// https://youtu.be/sBws8MSXN7A
import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Header from "./components/layout/Header";
import Todos from "./components/Todos";
import AddTodo from "./components/AddTodo";
import About from "./components/pages/About";
import { v4 as uuid } from "uuid"; //Hard Coded Version
import "./App.css";
import axios from "axios";

class App extends React.Component {
  state = {
    todos: [
      //Hard Coded Version:
      // {
      //   id: uuid(),
      //   title: "Take out the trash",
      //   completed: false,
      // },
      // {
      //   id: uuid(),
      //   title: "Dinner with wife",
      //   completed: true,
      // },
      // {
      //   id: uuid(),
      //   title: "Meeting with boss",
      //   completed: false,
      // },
    ],
  };

  //Non-Hard Coded Version
  componentDidMount() {
    axios
      .get("https://jsonplaceholder.typicode.com/todos?_limit=10")
      .then((res) => this.setState({ todos: res.data }));
  }

  // Toggle Complete
  markComplete = (id) => {
    this.setState({
      todos: this.state.todos.map((todo) => {
        if (todo.id === id) {
          todo.completed = !todo.completed; //opposite of what it was before
        }
        return todo;
      }),
    });
  };

  //Delete Todo
  delTodo = (id) => {
    //Dynamic Coded Version -  issue with CORS security in browser
    //axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
    //  .then(res => this.setState({ todos: [...this.state.todos.filter(todo => todo.id !== id)] }));

    //Hard Coded Version
    this.setState({
      todos: [...this.state.todos.filter((todo) => todo.id !== id)],
    }); //3 dots are the spread operator that copies current state
  };

  //Add Todo
  addTodo = (title) => {
    //Dynamic Coded Version -  issue with CORS security in browser
    // axios.post('https://jsonplaceholder.typicode.com/todos', {
    //   title,
    //   completed: false
    // })
    //   .then(res => { this.setState({ todos: [...this.state.todos, res.data ] });

    //Hard Coded Version:
    const newTodo = {
      id: uuid(),
      title, //ES6 shorthand for:   title: title,
      completed: false,
    };
    this.setState({ todos: [...this.state.todos, newTodo] });
  };

  render() {
    return (
      <Router>
        <div className="App">
          <div className="container">
            <Header />
            <Route exact path="/" render={(props) => (
                <React.Fragment>
                  <AddTodo addTodo={this.addTodo} />
                  <Todos
                    todos={this.state.todos}
                    markComplete={this.markComplete}
                    delTodo={this.delTodo}
                  />
                </React.Fragment>
              )}
            />
            <Route path="/about" component={About} />
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
